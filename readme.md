# Pipeline Gitlab pour Alpes Isère Habitat

Fichiers de références pour la mise à jour de l'image Docker utilisée pendant les tests d'intégration continue, et le fichier décrivant la pipeline .gitlab-ci.yml

Fichiers de référence pour la mise à jour de l'image Docker utilisée pour la réalisation des scans sonarqube.

Cette configuration est à l'usage de Alpes Isère Habitat pour tester des applications Symfony, mais peut être utilisée dans d'autre projets php (avec adaptation pour votre environnement).

Cette pipeline est prévue pour notre environnement de developpement, ou l'ensemble de nos applications Symfony tournent dans des containeurs Docker.

## Mettre à jour l'image pour les tests (/phpqa)

Editer le fichier Dockerfile, en ajoutant les modules et extentions manquantes.
Attention, l'image Docker est à base de Linux Alpine (plus leger), il faut donc utiliser apk (au lieu de apt sur Debian).

Il suffit ensuite de faire un build de l'image, et la publier sur le hub docker en lui précisant un numéro de version (à adapter avec vos infos du Hub Docker ):

```
$ docker build -t alpesiserehabitat/phpqa .
$ docker tag {id du build} alpesiserehabitat/phpqa:{Version}
$ docker login
$ docker push alpesiserehabitat/phpqa:{Version}
```

## Mettre à jour l'image pour Sonar Scanner (/sonar-scanner)

Il suffit ensuite de faire un build de l'image, et la publier sur le hub docker en lui précisant un numéro de version (à adapter avec vos infos du Hub Docker ):

```
$ docker build -t alpesiserehabitat/sonar-scanner .
$ docker tag {id du build} alpesiserehabitat/sonar-scanner:{Version}
$ docker login
$ docker push alpesiserehabitat/sonar-scanner:{Version}
```

## Utilisation de la Pipeline

Afin que la partie Déploiement Continu fonctionne, il faut dans le dépot GitLab du projet, dans la partie ["Settings > CI/CD > Variables"](https://docs.gitlab.com/ee/ci/variables/README.html#create-a-custom-variable-in-the-ui) définir les variables suivantes :

* SSH_USER_PASS : Le mot de pass de votre utilisateur SSH sur votre serveur hébergeant le conteneur Docker du projet
* SSH_USER : Le nom d'utilisateur de votre utilisteur SSH sur votre serveur hébergeant le conteneur Docker du projet
* SERVERNAME : L'adresse de votre serveur hébergeant le conteneur Docker du projet
* DOCKERNAME : Le nom du containeur (apache/php) hébergeant votre projet

## Utilisation de Sonar Scanner

Afin de lancer un scan Sonarqube du projet, il faut réaliser les actions suivantes:

* Ajouter un fichier sonar-project.properties à la racine du projet
* Ajouter "sonar-project.properties" au .gitignore à la racine projet
* Personaliser le fichier sonar-project.properties :
    * un modèle est dispo dans ce dépot dans le repertoire "sonar-scanner"
    * la clé s'obtient en créant le projet sur Snarqube
* Dans un terminal, à la racine du projet

```
$ docker run -v "$(pwd)":/src -it alpesiserehabitat/sonar-scanner:1.0
$ sonar-scanner
$ exit
```

## Built With

* [phpqa](https://github.com/jakzal/phpqa) - Docker image that provides static analysis tools for PHP
* [ubuntu](https://hub.docker.com/_/ubuntu) - Docker Official Images
* [sshpass](https://manpages.debian.org/stretch/sshpass/sshpass.1.en.html) - sshpass is a utility designed for running ssh using the mode referred to as "keyboard-interactive" password authentication, but in non-interactive mode.

## Authors

* **Yoan Bernabeu** - *pour Alpes Isère Habitat* - [Profil Gitlab](https://gitlab.com/yoan.bernabeu)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
